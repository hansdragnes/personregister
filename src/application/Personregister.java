package application;

import java.util.ArrayList;
import java.util.Iterator;

public class Personregister 
{
	private static int personer=0;
	private ArrayList<Person> pliste = new ArrayList<Person>();
	
	public Personregister()
	{
		
	}
	
	public int listeLength() {
		return pliste.size();
	}
	
	public boolean addPerson(Person p) {
		pliste.add(p);
		personer++;
		return true;
		
	}
	
	public Person getPerson(String etternavn) {
		Iterator<Person> itr = pliste.iterator();
		while (itr.hasNext()) {
			Person p = itr.next();
			if (p.getEtternavn().equals(etternavn)) {
				return p;
			}
		}
			return null;
	}
	
	public int getPersoner() {
		return personer; 
	}

}
