package application;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class Controller {

	protected Person person;
	protected Personregister pr = new Personregister();
	
    @FXML
    private TextField inputFirstName;

    @FXML
    private TextField inputAge;

    @FXML
    private TextField inputLastName;
    
    @FXML
    private TextField resultTextField;

    @FXML
    private Button inputSlctButton;

    @FXML
    private TextField outputFirstName;

    @FXML
    private TextField outputAge;

    @FXML
    private TextField outputLastName;

    @FXML
    private Button vispersonSlctButton;
    
    @FXML
    private TextField inputEtternavn;
    
    @FXML
    private Button resultSlctButton;
    
    
    

    @FXML
    void inputSlctButtonPressed(ActionEvent event) {
    	person= new Person(inputFirstName.getText(), inputLastName.getText(), Integer.parseInt(inputAge.getText()));
    	if (pr.addPerson(person))
    		resultTextField.setText(person.toString());
    	else
    		resultTextField.setText("Oppretting feilet");
    	
    	    	
    }

    @FXML
    void vispersonSlctButtonPressed(ActionEvent event) {
    	String etternavn = outputLastName.getText();
    	Person p = pr.getPerson(etternavn);
    	
    	if (p != null) {
    		System.out.println(p.toString());
    		outputFirstName.setText(p.getFornavn());
	    	outputLastName.setText(p.getEtternavn());
	    	outputAge.setText(Integer.toString(p.getAlder()));
    	}
    	
    }
    
    @FXML
    void resultSlctButtonPressed(ActionEvent event) {
    	System.out.println(person);
    }

}
