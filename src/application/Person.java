package application;

public class Person 
{
	private String fornavn;
	private String etternavn;
	private int alder;
	private static int antPersonobjekter=0;
	
	public Person(String fn, String en, int a)
	{
		fornavn=fn;
		etternavn=en;
		alder=a;
		antPersonobjekter++;
	}
	
	public String getFornavn()
	{
		return fornavn;
	}

	public String getEtternavn()
	{
		return etternavn;
	}

	
	public int getAlder()
	{
		return alder;
	}

	public int getantPersonobjekter()
	{
		return antPersonobjekter;
	}
	
	public String toString() 
	{
		return "Person [navn=" + fornavn + " "+etternavn+", alder=" + alder + "]";
	}

	public void setFornavn(String fn) {
		this.fornavn = fn;
	}
	public void setEtternavn(String en) {
		this.etternavn = en;
	}

	public void setAlder(int alder) {
		this.alder = alder;
	}
	
}
